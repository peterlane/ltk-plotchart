# ltk-plotchart

A wrapper around tklib's
[plotchart](https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html)
library to work with LTk / nodgui.

For complete documentation see <https://peterlane.codeberg.page/ltk-plotchart/>

# Getting Started: Install and Test

## Plotchart

Make sure you have [tcltk](https://www.tcl.tk/software/tcltk/) installed.

Plotchart is part of Tklib, which is available from the
[tklib](https://www.tcl.tk/software/tklib/) webpage. 

Linux/MacOS:: usually installable through the package manager, e.g. Ubuntu:

    > sudo apt install tklib

Windows:: download and unzip the "Zip Distribution Archive" from 
[tklib](https://core.tcl-lang.org/tklib/event/545a7ee378a0cc271fcf58ebf83684fb4257094e). 
Install by double-clicking on "installer.tcl".

## ltk-plotchart

Make sure you have either [LTk](http://www.peter-herth.de/ltk/) or
[nodgui](https://www.autistici.org/interzona/nodgui.html) installed.

To install ltk-plotchart,
[download](https://codeberg.org/peterlane/ltk-plotchart/releases) and make the
code available to the
[asdf](https://common-lisp.net/project/asdf/asdf/Configuring-ASDF-to-find-your-systems.html)
system (depending on your installation and setup, this is done by copying or
linking the downloaded code to the appropriate folder: e.g. on Linux,
`~/common-lisp/`, or on Windows, `~/AppData/Local/common-lisp/source`).

To test the internal functions (LTk only):

    * (asdf:test-system :ltk-plotchart)

## Examples

To run an example, cd to the appropriate folder within "ltk-plotchart/examples": 

    > sbcl --script plotdemos1.lisp

Examples tested on Linux using [sbcl](http://www.sbcl.org) version 2.1.3,
[ltk](https://github.com/herth/ltk/releases) version 0.992 and 
nodgui version 0.1.1 (via [quicklisp](https://www.quicklisp.org) with 
[tklib](https://core.tcl-lang.org/tklib/event/545a7ee378a0cc271fcf58ebf83684fb4257094e)
version 0.7.

# MIT License

Copyright (c) 2021, Peter Lane <peterlane@gmx.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

