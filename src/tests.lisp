(in-package :ltk-plotchart)

(defun check (query error-message)
  (if query
    (format t ".")
    (format t "~&Error: ~a~&" error-message)))

(defmacro check-no-error (expr msg)
  `(check (handler-case ,expr
            (error (c) (declare (ignore c))
                   nil))
          ,msg))

(defmacro check-error (expr msg)
  `(check (handler-case (prog2 ,expr nil)
            (error (c) (declare (ignore c))
                   t))
          ,msg))

(defun test-when-valid ()
  (check-no-error (string= "red" 
                           (when-valid :red '("red" "blue" "green") "colours"))
                  "when-valid: failed on valid :red")
  (check-no-error (string= "red" 
                           (when-valid "RED" '("red" "blue" "green") "colours"))
                  "when-valid: failed on valid 'RED'")
  (check-error (string= "red" 
                        (when-valid :red '("blue" "green") "colours"))
               "when-valid: failed on missing :red")
  )

(defun test-validators ()
  (check-no-error (valid-axis-p '(1 10 2) "a")
         "validators: fail on '(1 10 2)")
  (check-error (valid-axis-p '(10 2) "a")
         "validators: fail on '(a 10 2)")
  (check-error (valid-axis-p '(1 10 2 3) "a")
         "validators: fail on '(a 10 2)")
  (check-error (valid-axis-p '(a 10 2) "a")
         "validators: fail on '(a 10 2)")

  (check-error (valid-number-pair-p '(1 10 2) "a")
         "validators: fail on '(1 10 2)")
  (check-no-error (valid-number-pair-p '(10 2) "a")
         "validators: fail on '(a 10 2)")

  (check-error (valid-number-pair-p '(1 10 2) "a")
               "validators: fail on '(1 10 2) radius data")
  (check-no-error (valid-number-pair-p '(1 10) "a")
                  "validators: fail on '(1 10) radius data")

  (check-no-error (valid-time-axis-p '("start" "end" 10) "a")
                  "validators: fail on '(start end 10) time")
  (check-error (valid-time-axis-p '(10 "start" "end") "a")
                  "validators: fail on '(10 start end) time")
  (check-error (valid-time-axis-p '("start" 10 "end") "a")
                  "validators: fail on '(start 10 end) time")
  (check-error (valid-time-axis-p '("start" "end") "a")
                  "validators: fail on '(start end) time")
  )

(defun run-tests ()
  (format t "~&Testing: ") 
  (test-when-valid)
  (test-validators)
  (format t "~%-------- Done~&"))
