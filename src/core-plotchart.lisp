;; LTk wrapper around the tklib plotchart module
;;
;; -- definition and naming follows that in the plotchart documentation
;; https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html
;;
;; Not implemented (because ...):
;;
;; - chart-saveplot - option for :format requires Img library, so not supported
;; - $anyplot object itemtype series args
;; - $anyplot deletedata
;; - normal-plot - requires math::statistics package
;; - 3d-ribbon-chart - example fails in TCL code
;; - 3dplots - plotfunc, plotfuncont - not sure how to handle function (callback?)
;; - $table formatcommand procname - not sure how to handle procedures
;; - no options to plot-pack - &key and &rest ? 

;; ---------------------------------------------------------------------------
;; Classes for all supported plotchart types

;; Plotchart - parent class and common functions

(defvar *plot-count* 0)

(defun new-name ()
  (incf *plot-count*)
  (format nil "plotchart_~d" *plot-count*))

(defclass plotchart ()
  ((name :accessor name :initform "plotchart")
   (canvas :accessor canvas :initarg :canvas :initform nil)))

(defmethod initialize-instance :before ((chart plotchart) &key)
  (setf (name chart) (new-name)))

(defclass xy-plot-group (plotchart) ()) ; used to group methods, no initialisation

;; Bar Chart

(defclass bar-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk bar-chart instance. 

                  Constructor:: <<create-bar-chart-function>>"))

(defmethod initialize-instance :after ((chart bar-chart) &key xlabels yaxis num-series xlabel-angle)
  (when (valid-axis-p yaxis "bar-chart - yaxis")
    (format-wish "global ~a; set ~a [::Plotchart::createBarchart ~a {~{~a ~}} {~{~a ~}} ~(~a~)~@[ -xlabelangle ~a~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xlabels
                 yaxis
                 num-series
                 xlabel-angle)))

(defun create-bar-chart (canvas xlabels yaxis num-series &key xlabel-angle)
  "* `canvas` - parent canvas
  * `xlabels` - list of labels for the x-axis
  * `yaxis` - (min max step) axis definition
  * `num-series` - number of series to plot, or :stacked
  * `xlabel-angle` - angle at which to display x-axis labels

  Returns a new instance of a <<bar-chart-class>>."
  (make-instance 'bar-chart
                 :canvas canvas
                 :xlabels xlabels
                 :yaxis yaxis
                 :num-series num-series
                 :xlabel-angle xlabel-angle))

;; Box Plot

(defclass box-plot (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk box-chart instance. 

                  Constructor:: <<create-box-plot-function>>"))

(defmethod initialize-instance :after ((chart box-plot) &key xdata ydata orientation)
  (format-wish "global ~a; set ~a [::Plotchart::createBoxplot ~a {~{{~a} ~}} {~{{~a} ~}}~@[ ~a~]]" 
               (name chart)
               (name chart) 
               (widget-path (canvas chart))
               xdata
               ydata
               (and orientation
                    (when-valid orientation '("horizontal" "vertical") "orientation"))))

(defun create-box-plot (canvas xdata ydata &key (orientation :horizontal))
  "* `canvas` - parent canvas
  * `xdata` - either a numeric axis (min max step) or a list of labels
  * `ydata` - either a list of labels or a numeric axis (min max step) 
  * `orientation` - one of (:horizontal :vertical)

  When `orientation` is

  * `:horizontal` - `xdata` is a numeric axis and `ydata` a list of labels
  * `:vertical` - `xdata` is a list of labels and `ydata` a numeric axis.

  Returns a new instance of a <<box-plot-class>>.

  Error:: if xdata/ydata are not one axis and one list."
  (make-instance 'box-plot
                 :canvas canvas
                 :xdata xdata
                 :ydata ydata
                 :orientation orientation))

;; Gantt Chart

(defclass gantt-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk gantt-chart instance. 

                  Constructor:: <<create-gantt-chart-function>>"))

(defmethod initialize-instance :after ((chart gantt-chart) &key time-begin time-end num-items max-width bar-height ylabel-width)
  (format-wish "global ~a; set ~a [::Plotchart::createGanttchart ~a {~a} {~a} ~a ~a ~@[ -barheight ~d ~] ~@[ -ylabelwidth ~d ~]]"
               (name chart)
               (name chart)
               (widget-path (canvas chart))
               time-begin 
               time-end 
               (if num-items num-items (if max-width "1" "")) ; note, max-width must be second
               (if max-width max-width "")
               bar-height
               ylabel-width))

(defun create-gantt-chart (canvas time-begin time-end &key num-items max-width bar-height ylabel-width)
  "* `canvas` - parent canvas
  * `time-begin` - clock scan format - see https://www.tcl.tk/man/tcl8.3/TclCmd/clock.htm#M37[manual]
  * `time-end` - clock scan format - see https://www.tcl.tk/man/tcl8.3/TclCmd/clock.htm#M37[manual]
  * `num-items` - expected/maximum number of items (for spacing)
  * `max-width` - expected/maximum width of descriptive text (defaults to 20)
  * `bar-height` - in pixels, alternative way to specify spacing
  * `ylabel-width` - in pixels, space for y labels

  Returns a new instance of a <<gantt-chart-class>>."
  (make-instance 'gantt-chart
                 :canvas canvas
                 :time-begin time-begin
                 :time-end time-end
                 :num-items num-items
                 :max-width max-width
                 :bar-height bar-height
                 :ylabel-width ylabel-width))

;; Histogram 

(defclass histogram (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk histogram instance. 

                  Constructor:: <<create-histogram-function>>"))

(defmethod initialize-instance :after ((chart histogram) &key xaxis yaxis
                                                         xlabels ylabels box
                                                         axesbox)
  (when (and (valid-axis-p xaxis "histogram - xaxis")
             (valid-axis-p yaxis "histogram - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createHistogram ~a {~{~a ~}} {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 xlabels ylabels 
                 box axesbox)))

(defun create-histogram (canvas xaxis yaxis &key xlabels ylabels box axesbox)
  "* `canvas` - parent canvas
  * `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
  * `yaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
  * `xlabels` - custom labels for the x-axis
  * `ylabels` - custom labels for the y-axis
  * `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
  * `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]

  Returns a new instance of a <<histogram-class>>."
  (make-instance 'histogram
                 :canvas canvas
                 :xaxis xaxis
                 :yaxis yaxis
                 :xlabels xlabels
                 :ylabels ylabels
                 :box box
                 :axesbox axesbox))

;; Horizontal Bar Chart

(defclass horizontal-bar-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk horizontal-bar-chart instance. 

                  Constructor:: <<create-horizontal-bar-chart-function>>"))

(defmethod initialize-instance :after ((chart horizontal-bar-chart) &key xaxis ylabels num-series)
  (when (valid-axis-p xaxis "horizontal-bar-chart - xaxis")
    (format-wish "global ~a; set ~a [::Plotchart::createHorizontalBarchart ~a {~{~a ~}} {~{~a ~}} ~(~a~)]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 ylabels
                 num-series)))

(defun create-horizontal-bar-chart (canvas xaxis ylabels num-series)
  "* `canvas` - parent canvas
  * `xaxis` - (min max step) axis definition
  * `ylabels` - list of labels for the y-axis
  * `num-series` - number of series to plot, or :stacked

  Returns instance of a <<horizontal-bar-chart-class>>."
  (make-instance 'horizontal-bar-chart
                 :canvas canvas
                 :xaxis xaxis
                 :ylabels ylabels
                 :num-series num-series))

;; Isometric Plot

(defclass isometric-plot (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk isometric-plot instance. 

                  Constructor:: <<create-isometric-plot-function>>"))

(defmethod initialize-instance :after ((chart isometric-plot) &key xaxis yaxis stepsize)
  (when (and (valid-number-pair-p xaxis "isometric-plot - xaxis")
             (valid-number-pair-p yaxis "isometric-plot - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createIsometricPlot ~a~@[ {~{~a ~}} ~]~@[ {~{~a ~}} ~]~@[ ~(~a~) ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 stepsize)))

(defun create-isometric-plot (canvas xaxis yaxis stepsize)
  "* `canvas` - parent canvas
  * `xaxis` - (min max) axis definition
  * `yaxis` - (min max) axis definition
  * `stepsize` - either stepsize for the axes or :noaxes to not display the axes

  Returns a new instance of an <<isometric-plot-class>>."
  (make-instance 'isometric-plot
                 :canvas canvas
                 :xaxis xaxis
                 :yaxis yaxis
                 :stepsize stepsize))

;; LogX-LogY Plot
;; - XY Plot with both axes having logarithmic values

(defclass logx-logy-plot (xy-plot-group)
  ()
  (:documentation "Instance holds pointer to tcl/tk logx-logy-plot instance. 

                  Constructor:: <<create-logx-logy-plot-function>>"))

(defmethod initialize-instance :after ((chart logx-logy-plot) &key 
                                                              xaxis yaxis
                                                              xlabels ylabels box
                                                              axesbox timeformat gmt
                                                              axesatzero isometric)
  (when (and (valid-axis-p xaxis "logx-logy-plot - xaxis")
             (valid-axis-p yaxis "logx-logy-plot - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createLogXLogYPlot ~a {~{~a ~}} {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -timeformat {~a} ~]~@[ -gmt ~a ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 xlabels ylabels box axesbox 
                 timeformat gmt 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-logx-logy-plot (canvas xaxis yaxis &key xlabels ylabels box
                                     axesbox timeformat gmt
                                     axesatzero isometric)
  "* `canvas` - parent canvas
* `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `yaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `xlabels` - custom labels for the x-axis
* `ylabels` - custom labels for the y-axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `timeformat` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `gmt` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of a <<logx-logy-plot-class>>."
(make-instance 'logx-logy-plot
               :canvas canvas
               :xaxis xaxis
               :yaxis yaxis
               :xlabels xlabels
               :ylabels ylabels
               :box box
               :axesbox axesbox
               :timeformat timeformat
               :gmt gmt
               :axesatzero axesatzero
               :isometric isometric))

;; LogX-Y Plot
;; - XY Plot with x axis having logarithmic values

(defclass logx-y-plot (xy-plot-group)
  ()
  (:documentation "Instance holds pointer to tcl/tk logx-y-plot instance. 

                  Constructor:: <<create-logx-y-plot-function>>"))

(defmethod initialize-instance :after ((chart logx-y-plot) &key 
                                                           xaxis yaxis
                                                           xlabels ylabels box
                                                           axesbox timeformat gmt
                                                           axesatzero isometric)
  (when (and (valid-axis-p xaxis "logx-y-plot - xaxis")
             (valid-axis-p yaxis "logx-y-plot - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createLogXYPlot ~a {~{~a ~}} {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -timeformat {~a} ~]~@[ -gmt ~a ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 xlabels ylabels box axesbox 
                 timeformat gmt 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-logx-y-plot (canvas xaxis yaxis &key xlabels ylabels box
                                  axesbox timeformat gmt
                                  axesatzero isometric)
  "* `canvas` - parent canvas
* `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `yaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `xlabels` - custom labels for the x-axis
* `ylabels` - custom labels for the y-axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `timeformat` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `gmt` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of a <<logx-y-plot-class>>."
(make-instance 'logx-y-plot
               :canvas canvas
               :xaxis xaxis
               :yaxis yaxis
               :xlabels xlabels
               :ylabels ylabels
               :box box
               :axesbox axesbox
               :timeformat timeformat
               :gmt gmt
               :axesatzero axesatzero
               :isometric isometric))

;; Pie Chart

(defclass pie-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk pie-chart instance. 

                  Constructor:: <<create-pie-chart-function>>"))

(defmethod initialize-instance :after ((chart pie-chart) &key)
  (format-wish "global ~a; set ~a [::Plotchart::createPiechart ~a]" 
               (name chart) (name chart) (widget-path (canvas chart))))

(defun create-pie-chart (canvas)
  "* `canvas` - parent canvas
  
  Returns a new instance of a <<pie-chart-class>>."
  (make-instance 'pie-chart :canvas canvas))

;; Polar Plot

(defclass polar-plot (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk polar-plot instance. 

                  Constructor:: <<create-polar-plot-function>>"))

(defmethod initialize-instance :after ((chart polar-plot) &key radius-data
                                                          xlabels ylabels box
                                                          axesbox axesatzero isometric)
  (when (valid-number-pair-p radius-data "polar-plot - radius-data")
    (format-wish "global ~a; set ~a [::Plotchart::createPolarplot ~a {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 radius-data
                 xlabels ylabels box axesbox 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-polar-plot (canvas radius-data &key xlabels ylabels box axesbox
                                 axesatzero isometric)
  "* `canvas` - parent canvas
* `radius-data` - (max-radius, step-size) definition for radial axis
* `xlabels` - custom labels for the x-axis
* `ylabels` - custom labels for the y-axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of <<polar-plot-class>>."
(make-instance 'polar-plot
               :canvas canvas
               :radius-data radius-data
               :xlabels xlabels
               :ylabels ylabels
               :box box
               :axesbox axesbox
               :axesatzero axesatzero
               :isometric isometric))

;; Radial Chart

(defclass radial-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk radial-chart instance. 

                  Constructor:: <<create-radial-chart-function>>"))

(defmethod initialize-instance :after ((chart radial-chart) &key names scale style)
  (format-wish "global ~a; set ~a [::Plotchart::createRadialchart ~a {~{{~a} ~}} ~f~@[ ~a~]]" 
               (name chart)
               (name chart) 
               (widget-path (canvas chart))
               names
               scale 
               (and style
                    (when-valid style '("lines" "cumulative" "filled") "radial-chart - style"))))

(defun create-radial-chart (canvas names scale &key (style "lines"))
  "* `canvas` - parent canvas
  * `names` - list of names for the spokes
  * `scale` - determines position of data on spokes
  * `style` - one of (:lines :cumulative :filled)

  Returns a new instance of a <<radial-chart-class>>."
  (make-instance 'radial-chart
                 :canvas canvas
                 :names names
                 :scale scale
                 :style style))

;; Right Axis

(defclass right-axis (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk right-axis instance. 

                  Constructor:: <<create-right-axis-function>>"))

(defmethod initialize-instance :after ((chart right-axis) &key yaxis)
  (when (valid-axis-p yaxis "right-axis - yaxis")
    (format-wish "global ~a; set ~a [::Plotchart::createRightAxis ~a {~{~a ~}}]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 yaxis)))

(defun create-right-axis (canvas yaxis)
  "* `canvas` - parent canvas
  * `yaxis` - (min max step) axis definition

  Returns a new instance of a <<right-axis-class>>."
  (make-instance 'right-axis :canvas canvas :yaxis yaxis))

;; Spiral Pie 

(defclass spiral-pie (plotchart) 
  ()
  (:documentation "Instance holds pointer to tcl/tk spiral-pie instance. 

                  Constructor:: <<create-spiral-pie-function>>"))

(defmethod initialize-instance :after ((chart spiral-pie) &key)
  (format-wish "global ~a; set ~a [::Plotchart::createSpiralPie ~a]" 
               (name chart) (name chart) (widget-path (canvas chart))))

(defun create-spiral-pie (canvas)
  "* `canvas` - parent canvas
  
  Returns a new instance of a <<spiral-pie-class>>."
  (make-instance 'spiral-pie :canvas canvas))

;; Status timeline

(defclass status-timeline (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk status-timeline instance. 

                  Constructor:: <<create-status-timeline-function>>"))

(defmethod initialize-instance :after ((chart status-timeline) &key xaxis ylabels box axesbox showxaxis)
  (when (valid-axis-p xaxis "status-timeline - xaxis")
    (format-wish "global ~a; set ~a [::Plotchart::createStatusTimeline ~a {~{~a ~}} {~{{~a} ~}}~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -xaxis ~a ~]]"
                 (name chart)
                 (name chart)
                 (widget-path (canvas chart))
                 xaxis
                 ylabels
                 box axesbox
                 (if showxaxis "1" "0"))))

(defun create-status-timeline (canvas xaxis ylabels &key box axesbox showxaxis)
  "* `canvas` - parent canvas
  * `xaxis` - (min, max, step) axis definition
  * `ylabels` - list of labels for the y-axis
  * `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
  * `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
  * `showxaxis` - T/nil, to display xaxis

  Returns a new instance of <<status-timeline-class>>."
  (make-instance 'status-timeline
                 :canvas canvas
                 :xaxis xaxis
                 :ylabels ylabels
                 :box box
                 :axesbox axesbox
                 :showxaxis showxaxis))

;; Strip Chart

(defclass strip-chart (xy-plot-group)
  ()
  (:documentation "Instance holds pointer to tcl/tk strip-chart instance. 

                  Constructor:: <<create-strip-chart-function>>"))

(defmethod initialize-instance :after ((chart strip-chart) &key xaxis yaxis 
                                                           xlabels ylabels box
                                                           axesbox timeformat gmt
                                                           axesatzero isometric)
  (when (and (valid-axis-p xaxis "strip-chart - xaxis")
             (valid-axis-p yaxis "strip-chart - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createStripchart ~a {~{~a ~}} {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -timeformat {~a} ~]~@[ -gmt ~a ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 xlabels ylabels box axesbox 
                 timeformat gmt 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-strip-chart (canvas xaxis yaxis &key xlabels ylabels box axesbox
                                  timeformat gmt axesatzero isometric)
  "* `canvas` - parent canvas
* `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `yaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `xlabels` - custom labels for the x-axis
* `ylabels` - custom labels for the y-axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `timeformat` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `gmt` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of a <<strip-chart-class>>."
(make-instance 'strip-chart
               :canvas canvas
               :xaxis xaxis
               :yaxis yaxis
               :xlabels xlabels
               :ylabels ylabels
               :box box
               :axesbox axesbox
               :timeformat timeformat
               :gmt gmt
               :axesatzero axesatzero
               :isometric isometric))

;; Table Chart

(defclass table-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk table-chart instance. 

                  Constructor:: <<create-table-chart-function>>"))

(defmethod initialize-instance :after ((chart table-chart) &key columns widths)
  (if (and widths (listp widths)) ; widths may be a list of values, or a single number
    (format-wish "global ~a; set ~a [::Plotchart::createTableChart ~a {~{{~a} ~}} {~{~a ~}}]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 columns
                 widths)
    (format-wish "global ~a; set ~a [::Plotchart::createTableChart ~a {~{{~a} ~}}~@[ ~a~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 columns
                 widths)))

(defun create-table-chart (canvas columns &key widths)
  "* `canvas` - parent canvas
  * `columns` - list of strings for the column headings
  * `widths` - either a column-wise list or single value

  Returns a new instance of a <<table-chart-class>>."
  (make-instance 'table-chart
                 :canvas canvas
                 :columns columns
                 :widths widths))

;; Ternary Diagram

(defclass ternary-diagram (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk ternary-diagram instance. 

                  Constructor:: <<create-ternary-diagram-function>>"))

(defmethod initialize-instance :after ((chart ternary-diagram) &key box axesbox fractions steps)
  (format-wish "global ~a; set ~a [::Plotchart::createTernaryDiagram ~a~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -fractions ~a ~]~@[ -steps ~a ~]]"
               (name chart)
               (name chart)
               (widget-path (canvas chart))
               box axesbox 
               (and fractions (if fractions "1" "0"))
               steps))

(defun create-ternary-diagram (canvas &key box axesbox fractions steps)
  "* `canvas` - parent canvas
  * `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
  * `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
  * `fractions` - T/nil to display 0/1 instead of 0/100%
  * `steps` - number of labels to use on each side

  Returns a new instance of a <<ternary-diagram-class>>."
  (make-instance 'ternary-diagram
                 :canvas canvas
                 :box box
                 :axesbox axesbox
                 :fractions fractions
                 :steps steps))

;; 3D Bar Chart

(defclass threed-bar-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk threed-bar-chart instance. 

                  Constructor:: <<create-3d-bar-chart-function>>"))

(defmethod initialize-instance :after ((chart threed-bar-chart) &key yaxis (num-bars 1))
  (when (valid-axis-p yaxis "threed-bar-chart - yaxis")
    (format-wish "global ~a; set ~a [::Plotchart::create3DBarchart ~a {~{~a ~}} ~d]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 yaxis
                 num-bars)))

(defun create-3d-bar-chart (canvas yaxis &key (num-bars 1))
  "* `canvas` - parent canvas
  * `yaxis` - (min max step) axis definition
  * `num-bars` - number of bars to plot

  Returns instance of a <<threed-bar-chart-class>>."
  (make-instance 'threed-bar-chart
                 :canvas canvas
                 :yaxis yaxis
                 :num-bars num-bars))

;; 3D Plot

(defclass threed-plot (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk threed-plot instance. 

                  Constructor:: <<create-3d-plot-function>>"))

(defmethod initialize-instance :after ((chart threed-plot) &key xaxis yaxis zaxis xlabels)
  (when (and (valid-axis-p xaxis "3d-plot - xaxis")
             (valid-axis-p yaxis "3d-plot - yaxis")
             (valid-axis-p zaxis "3d-plot - zaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::create3DPlot ~a {~{~a ~}} {~{~a ~}} {~{~a ~}}~@[ {~{ {~a}~} }~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 zaxis
                 xlabels)))

(defun create-3d-plot (canvas xaxis yaxis zaxis &key xlabels)
  "* `canvas` - parent canvas
  * `xaxis` - (min, max, step) axis definition
  * `yaxis` - (min, max, step) axis definition
  * `zaxis` - (min, max, step) axis definition
  * `xlabels` - list of labels for the x-axis

  Returns a new instance of a <<threed-plot-class>>."
  (make-instance 'threed-plot
                 :canvas canvas
                 :xaxis xaxis
                 :yaxis yaxis
                 :zaxis zaxis
                 :xlabels xlabels))

;; 3D Ribbon Plot

(defclass threed-ribbon-plot (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk threed-ribbon-plot instance. 

                  Constructor:: <<create-3d-ribbon-plot-function>>"))

(defmethod initialize-instance :after ((chart threed-ribbon-plot) &key yaxis zaxis)
  (when (and (valid-axis-p yaxis "threed-ribbon-plot - yaxis")
             (valid-axis-p zaxis "threed-ribbon-plot - zaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::create3DRibbonPlot ~a {~{~a ~}} {~{~a ~}}]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 yaxis
                 zaxis)))

(defun create-3d-ribbon-plot (canvas yaxis zaxis)
  "* `canvas` - parent canvas
  * `yaxis` - (min, max, step) axis definition
  * `zaxis` - (min, max, step) axis definition

  Returns a new instance of a <<threed-ribbon-plot-class>>."
  (make-instance 'threed-ribbon-plot
                 :canvas canvas
                 :yaxis yaxis
                 :zaxis zaxis))

;; Time Chart

(defclass time-chart (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk time-chart instance. 

                  Constructor:: <<create-time-chart-function>>"))

(defmethod initialize-instance :after ((chart time-chart) &key time-begin time-end num-items bar-height ylabel-width)
  (format-wish "global ~a; set ~a [::Plotchart::createTimechart ~a {~a} {~a} ~a~a~a]"
               (name chart)
               (name chart)
               (widget-path (canvas chart))
               time-begin 
               time-end 
               (if num-items num-items "")
               (if bar-height (format nil " -barheight ~d" bar-height) "")
               (if ylabel-width (format nil " -ylabelwidth ~d" ylabel-width) "")))

(defun create-time-chart (canvas time-begin time-end &key num-items bar-height ylabel-width)
  "* `canvas` - parent canvas
  * `time-begin` - clock scan format - see https://www.tcl.tk/man/tcl8.3/TclCmd/clock.htm#M37[manual]
  * `time-end` - clock scan format - see https://www.tcl.tk/man/tcl8.3/TclCmd/clock.htm#M37[manual]
  * `num-items` - expected/maximum number of items (for spacing)
  * `bar-height` - in pixels, alternative way to specify spacing
  * `ylabel-width` - in pixels, space for y labels

  Returns a new instance of a <<time-chart-class>>."
  (make-instance 'time-chart 
                 :canvas canvas
                 :time-begin time-begin
                 :time-end time-end
                 :num-items num-items
                 :bar-height bar-height
                 :ylabel-width ylabel-width))

;; TX Plot

(defclass tx-plot (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk tx-plot instance. 

                  Constructor:: <<create-tx-plot-function>>"))

(defmethod initialize-instance :after ((chart tx-plot) &key timeaxis xaxis
                                                       box
                                                       axesbox timeformat gmt
                                                       axesatzero isometric)
  (when (and (valid-time-axis-p timeaxis "tx-plot - timeaxis")
             (valid-axis-p xaxis "tx-plot - xaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createTXPlot ~a {~{~a ~}} {~{~a ~}}~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -timeformat {~a} ~]~@[ -gmt ~a ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 timeaxis
                 xaxis
                 box axesbox 
                 timeformat gmt 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-tx-plot (canvas timeaxis xaxis &key box
                              axesbox timeformat gmt
                              axesatzero isometric)
  "* `canvas` - parent canvas
* `timeaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `timeformat` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `gmt` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of a <<tx-plot-class>>."
(make-instance 'tx-plot
               :canvas canvas
               :timeaxis timeaxis
               :xaxis xaxis
               :box box
               :axesbox axesbox
               :timeformat timeformat
               :gmt gmt
               :axesatzero axesatzero
               :isometric isometric))

;; Windrose

(defclass windrose (plotchart)
  ()
  (:documentation "Instance holds pointer to tcl/tk windrose instance. 

                  Constructor:: <<create-windrose-function>>"))

(defmethod initialize-instance :after ((chart windrose) &key radius-data (num-sectors 16))
  (when (valid-number-pair-p radius-data "windrose - radius-data")
    (format-wish "global ~a; set ~a [::Plotchart::createWindRose ~a {~{~a ~}} ~d]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 radius-data
                 num-sectors)))

(defun create-windrose (canvas radius-data &key (num-sectors 16))
  "* `canvas` - parent canvas
  * `radius-data` - (min, max) radius definition 
  * `num-sectors` - number of sectors to be plotted

  Returns a new instance of a <<windrose-class>>."
  (make-instance 'windrose
                 :canvas canvas
                 :radius-data radius-data
                 :num-sectors num-sectors))

;; X-LogY Plot
;; - XY Plot with y axis having logarithmic values

(defclass x-logy-plot (xy-plot-group)
  ()
  (:documentation "Instance holds pointer to tcl/tk x-logy-plot instance.
                  
                  Constructor:: <<create-x-logy-plot-function>>"))

(defmethod initialize-instance :after ((chart x-logy-plot) &key 
                                                           xaxis yaxis
                                                           xlabels ylabels box
                                                           axesbox timeformat gmt
                                                           axesatzero isometric)
  (when (and (valid-axis-p xaxis "x-logy-plot - xaxis")
             (valid-axis-p yaxis "x-logy-plot - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createXLogYPlot ~a {~{~a ~}} {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -timeformat {~a} ~]~@[ -gmt ~a ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 xlabels ylabels box axesbox 
                 timeformat gmt 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-x-logy-plot (canvas xaxis yaxis &key xlabels ylabels box
                                  axesbox timeformat gmt
                                  axesatzero isometric)
  "* `canvas` - parent canvas
* `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `yaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `xlabels` - custom labels for the x-axis
* `ylabels` - custom labels for the y-axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `timeformat` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `gmt` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of an <<x-logy-plot-class>>."
  (make-instance 'x-logy-plot
                 :canvas canvas
                 :xaxis xaxis
                 :yaxis yaxis
                 :xlabels xlabels
                 :ylabels ylabels
                 :box box
                 :axesbox axesbox
                 :timeformat timeformat
                 :gmt gmt
                 :axesatzero axesatzero
                 :isometric isometric))

;; XY Plot

(defclass xy-plot (xy-plot-group) 
  ()
  (:documentation "Instance holds pointer to tcl/tk xy-plot instance.

                  Constructor:: <<create-xy-plot-function>>"))

(defmethod initialize-instance :after ((chart xy-plot) &key 
                                                       xaxis yaxis
                                                       xlabels ylabels box
                                                       axesbox timeformat gmt
                                                       axesatzero isometric)
  (when (and (valid-axis-p xaxis "xy-plot - xaxis")
             (valid-axis-p yaxis "xy-plot - yaxis"))
    (format-wish "global ~a; set ~a [::Plotchart::createXYPlot ~a {~{~a ~}} {~{~a ~}}~@[ -xlabels {~{~a ~}} ~]~@[ -ylabels {~{~a ~}} ~]~@[ -box {~{~a ~}} ~]~@[ -axesbox {~{~a ~}} ~]~@[ -timeformat {~a} ~]~@[ -gmt ~a ~]~@[ -axesatzero ~a ~]~@[ -isometric ~a ~]]" 
                 (name chart)
                 (name chart) 
                 (widget-path (canvas chart))
                 xaxis
                 yaxis
                 xlabels ylabels box axesbox 
                 timeformat gmt 
                 (and axesatzero (if axesatzero "1" "0"))
                 (and isometric (if isometric "1" "0")))))

(defun create-xy-plot (canvas xaxis yaxis &key xlabels ylabels box
                              axesbox timeformat gmt
                              axesatzero isometric)
  "* `canvas` - parent canvas
* `xaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `yaxis` - (min, max, step) axis definition or (max, min, -step) for inverted axis
* `xlabels` - custom labels for the x-axis
* `ylabels` - custom labels for the y-axis
* `box` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `axesbox` - used with plot-pack - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section17[manual]
* `timeformat` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `gmt` - designed for strip-charts - see https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#section16[manual]
* `axesatzero` - T/nil to draw axes at (0,0) or in bottom-left corner
* `isometric` - T/nil to rescale axes so a square appears as a square

Returns a new instance of an <<xy-plot-class>>."
(make-instance 'xy-plot
               :canvas canvas
               :xaxis xaxis
               :yaxis yaxis
               :xlabels xlabels
               :ylabels ylabels
               :box box
               :axesbox axesbox
               :timeformat timeformat
               :gmt gmt
               :axesatzero axesatzero
               :isometric isometric))

;; ---------------------------------------------------------------------------
;; Functions

(defgeneric add-milestone (chart time-point &optional colour)
            (:documentation
              "* `chart` - <<time-chart-class>> to add milestone to
              * `time-point` - at which the milestone must be positioned
              * `colour` - of the triangle (defaults to black)

              Adds another milestone to the current row of a <<time-chart-class>>."))
(defmethod add-milestone ((chart time-chart) time-point &optional (colour :black))
  (format-wish "global ~a; $~a addmilestone {~a} ~(~a~)" 
               (name chart) (name chart) time-point colour))

(defgeneric add-period (chart time-begin time-end &optional colour)
            (:documentation
              "* `chart` - <<time-chart-class>> to add period to
              * `time-begin` - when period starts
              * `time-end` - when period ends
              * `colour` - of the triangle (defaults to black)

              Adds another new time period to the current row of a <<gantt-chart-class>> or <<time-chart-class>>."))
(defmethod add-period ((chart time-chart) time-begin time-end &optional (colour :black))
  (format-wish "global ~a; $~a addperiod {~a} {~a} ~(~a~)" 
               (name chart) (name chart) time-begin time-end
               colour))

(defgeneric add-row (chart items)
            (:documentation
              "* `chart` - <<table-chart-class>> to add row to
              * `items` - list of text items to add, one per column

              Adds a row of information to a <<table-chart-class>>."))
(defmethod add-row ((chart table-chart) items)
  (format-wish "global ~a; $~a row {~{{~a} ~}}" (name chart) (name chart) items))

(defgeneric background (chart part colour-or-image &optional direction brightness)
            (:documentation "* `chart` - to modify
                            * `part` - one of (:axes :plot :gradient :image)
                            * `colour-or-image` - either a colour name, or an image reference
                            * `direction` - one of (:top-down :bottom-up :left-right :right-left)
                            * `brightness` - one of (:bright :dark)

                            Sets the background of a part of the plot"))
(defmethod background ((chart plotchart) part colour-or-image &optional (direction :top-down) (brightness :bright))
  (format-wish "global ~a; $~a background ~a ~(~a~) ~(~a~) ~(~a~)"
               (name chart) 
               (name chart) 
               (when-valid part '("axes" "plot" "gradient" "image") "background - part")
               (if (eql :image part)
                 (widget-path colour-or-image)
                 colour-or-image)
               (when-valid direction 
                           '("top-down" "bottom-up" "left-right" "right-left")
                           "background - direction")
               (when-valid brightness
                           '("bright" "dark")
                           "background - brightness")))

(defgeneric balloon (chart x y text direction)
            (:documentation "* `chart` - to modify
                            * `x` - x-coordinate to point to
                            * `y` - y-coordinate to point to
                            * `text` - to display
                            * `direction` - direction of the _arrow_, one of (:north :north-east :east
                                                                                      :south-east :south :south-west :west :north-west)

                            Adds balloon text to the plot, with pointer to given coordinates.

                            NOTE: not supported for 3D charts."))
(defmethod balloon ((chart plotchart) x y text direction)
  (format-wish "global ~a; $~a balloon ~f ~f {~a} ~@[~a~]" 
               (name chart) (name chart) x y text 
               (and direction
                    (when-valid direction 
                                '("north" "north-east" "east" "south-east" "south"
                                  "south-west" "west" "north-west")
                                "direction"))))

(defgeneric balloon-config (chart &key font justify textcolour textcolor 
                                  background outline margin rimwidth arrowsize)
            (:documentation "* `chart` - to modify
            * `font` - name of font
            * `justify` - one of (:left :center :right)
            * `textcolour` `textcolor` - name of text colour
            * `background` - name of background colour
            * `outline` - name of outline colour
            * `margin` - in pixels, margin around text
            * `rimwidth` - in pixels, width of outline
            * `arrowsize` - in pixels, arrow length

            Configures balloon text for given plot - settings apply to next call to chart-balloon"))
(defmethod balloon-config ((chart plotchart) &key font justify textcolour 
                                             textcolor background outline margin 
                                             rimwidth arrowsize)
  (format-wish "global ~a; $~a balloonconfig~@[ -font {~a} ~]~@[ -justify ~a ~]~@[ -textcolour ~a ~]~@[ -background ~a ~]~@[ -outline ~a ~]~@[ -margin ~a ~]~@[ -rimwidth ~a ~]~@[ -arrowsize ~a ~]" 
               (name chart) (name chart)
               font 
               (and justify
                    (when-valid justify 
                                '("left" "center" "right") 
                                "balloon-config - justify"))
               (or textcolour textcolor)
               background
               outline
               margin
               rimwidth
               arrowsize))

(defgeneric cell-configure (chart &key background colour color font anchor justify)
            (:documentation "* `chart` - <<table-chart-class>> to modify
                            * `background` - colour
                            * `colour` `color` - colour of text (foreground)
                            * `font` - name of font
                            * `anchor` - how to position text in cell
                            * `justify` - layout of multiline text

                            Sets properties for displaying cells in subsequent rows."))
(defmethod cell-configure ((chart table-chart) &key background colour color font anchor justify)
  (format-wish "global ~a; $~a cellconfigure~@[ -background ~a ~]~@[ -color ~a ~]~@[ -font {~a} ~]~@[ -anchor ~a ~]~@[ -justify ~a ~]"
               (name chart) (name chart)
               background (or colour color) font anchor justify))

(defgeneric color (chart parameter-1 parameter-2)
            (:documentation "See <<colour-function>>."))
(defmethod color (chart parameter-1 parameter-2)
  (colour chart parameter-1 parameter-2))

(defgeneric colors (chart &rest colours)
            (:documentation "See <<colours-function>>."))
(defmethod colors (chart &rest colours)
  (apply #'colours chart colours))

(defun color-map (colours)
  "See <<colour-map-function>>."
  (colour-map colours))

(defgeneric colour (chart parameter-1 parameter-2)
            (:documentation "* `chart` - to modify

                            For <<gantt-chart-class>>:: colour (*chart* *keyword* *newcolour*)

                            * `keyword` - part of chart to change - one of (:description :completed :left :odd :even :summary :summarybar)
                            * `newcolour` - colour for chosen part

                            Sets colour for a part of gantt chart.

                            For <<threed-plot-class>>:: colour (*chart* *fill-colour* *border-colour*)

                            * `fill-colour` - colour for filling 
                            * `border-colour` - colour for border

                            Sets colour to use for polygon borders and interiors.
                            "))
(defmethod colour ((chart threed-plot) fill-colour border-colour)
  "Sets the fill and border colour"
  (format-wish "global ~a; $~a colour ~(~a~) ~(~a~)" 
               (name chart) (name chart) fill-colour border-colour))

(defmethod colour ((chart gantt-chart) keyword newcolour)
  (format-wish "global ~a; $~a colour ~a ~(~a~)" 
               (name chart) (name chart)
               (when-valid keyword ; required parameter
                           '("description" "completed" "left" "odd" "even" "summary" "summarybar")
                           "keyword")
               newcolour))

(defun colours (chart &rest colours)
  "* `chart` - to modify
  * `colour-1 colour-2 ...` - the colours to use for the slices/spokes

  Sets the colours for the pie slices or radial chart spokes.

  Error:: if chart type is not <<pie-chart-class>> or <<spiral-pie-class>>."
  (typecase chart
    ((or pie-chart spiral-pie)
     (format-wish "global ~a; $~a colours ~{~(~a~) ~}" 
                  (name chart) (name chart) colours))
    (otherwise
      (error "Unknown type ~a passed to colours" (type-of chart)))))

(defun colour-map (colours)
  "* `colours` - a list of colours, or one of pre-defined maps - (:grey :jet :cool :hot)

  Sets the colours to use with the contour map methods: list or designator"
  (if (listp colours)
    (format-wish "::Plotchart::colorMap {~{~(~a~)~}}" colours)
    (format-wish "::Plotchart::colorMap ~a" 
                 (when-valid colours 
                             '("cool" "gray" "grey" "jet" "hot") 
                             "colour-map - colours"))))

(defun config (chart &key show-values value-font value-colour value-format
                     use-background use-ticklines
                     label-font label-colour)
  "* `chart` - to modify
* `show-values` - T/nil, whether to show the values on top of bar or not
* `value-font` - font for showing values
* `value-colour` - colour for showing values

Option for <<bar-chart-class>> and <<horizontal-bar-chart-class>>:

* `value-format` - format string for showing values 

Options for <<threed-bar-chart-class>>:

* `use-background` - T/nil, whether to show left and back walls
* `use-ticklines` - T/nil, whether to show ticklines
* `label-font` - font for labels
* `label-colour` - colour for labels

Sets configuration parameters for bar charts.

Error:: if `chart` is not one of <<bar-chart-class>>, <<horizontal-bar-chart-class>> or <<threed-bar-chart-class>>."
(typecase chart
  ((or bar-chart horizontal-bar-chart)
   (when (or use-background use-ticklines label-font label-colour)
     (error "Unknown option given to config for chart type ~a" (type-of chart)))
   (format-wish "global ~a; $~a config~@[ -showvalues ~a ~]~@[ -valuefont {~a} ~]~@[ -valuecolour ~(~a~) ~]~@[ -valueformat {~a}~]"
                (name chart) (name chart)
                (and show-values (if show-values "1" "0"))
                value-font
                value-colour
                value-format))
  (threed-bar-chart
    (when value-format
      (error "Unknown option given to config for chart type ~a" (type-of chart)))
    (format-wish "global ~a; $~a config~@[ -showvalues ~a ~]~@[ -valuefont {~a} ~]~@[ -valuecolour ~(~a~) ~]~@[ -usebackground ~a ~]~@[ -useticklines ~a~]~@[ -labelfont {~a}~]~@[ -labelcolour ~(~a~)~]"
                 (name chart) (name chart)
                 (and show-values (if show-values "1" "0"))
                 value-font
                 value-colour
                 (and use-background (if use-background "1" "0"))
                 (and use-ticklines (if use-ticklines "1" "0"))
                 label-font
                 label-colour))
  (otherwise
    (error "Unknown chart type ~a passed to config" (type-of chart)))))

(defgeneric connect (chart from to)
            (:documentation "* `chart` - <<gantt-chart-class>> to modify
                            * `from` - task to connect from
                            * `to` - task to connect to

                            Connects task `from` to task `to`. The tasks are the return value from <<task-function>>."))
(defmethod connect ((chart gantt-chart) from to)
  (format-wish "global ~a; $~a connect $~a $~a" 
               (name chart) (name chart) from to))

(defgeneric corner-text (chart xtext ytext ztext)
            (:documentation "* `chart` - <<ternary-diagram-class>> to modify
                            * `xtext` - text to display in bottom left corner
                            * `ytext` - text to display in bottom right corner
                            * `ztext` - text to display in top center corner

                            Displays given texts at each corner"))
(defmethod corner-text ((chart ternary-diagram) xtext ytext ztext)
  (format-wish "global ~a; $~a text {~a} {~a} {~a}"
               (name chart) (name chart) xtext ytext ztext))

(defun data-config (chart series &key colour color type symbol radius width filled fillcolour style smooth
                          boxwidth whiskers whiskerwidth mediancolour medianwidth)
  "Sets the configuration for drawing of data in a given series, for specified chart types.

* `chart` - to modify
* `series` - to configure

Options for <<box-plot-class>>:

* `boxwidth` - width (or height) of the box
* `whiskers` - one of (:extremes :iqr :none) for how to draw whiskers
* `whiskerwidth` - line thickness 
* `mediancolour` - colour of median line
* `medianwidth` - thickness of median line

Options for <<histogram-class>> <<polar-plot-class>> <<right-axis-class>> <<strip-chart-class>> <<ternary-diagram-class>> <<tx-plot-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>:

* `colour` `color` - colour of data series
* `type` - drawing mode, one of (:line :symbol :both)
* `symbol` - when used, one of (:plus :cross :circle :up (triangle pointing
  up) :down (triangle pointing down) :dot (filled circle) :upfilled :downfilled)
* `radius` - of symbol
* `width` - of line
* `filled` - whether to fill area above/below line, one of (:no :up :down)
* `fillcolour` - colour to use in filling area

Options for <<histogram-class>> also include:

* `style` - for drawing histogram - one of (:filled :spike :plateau :stair)

Options for <<ternary-diagram-class>> also include:

* `smooth` - T/nil to use rounded corners

Error:: if `chart` type is not recognised."
(typecase chart
  ((or box-plot histogram polar-plot right-axis ternary-diagram tx-plot xy-plot-group)
   (format-wish "global ~a; $~a dataconfig ~a~@[ -colour ~(~a~) ~]~@[ -type ~(~a~) ~]~@[ -symbol ~(~a~) ~]~@[ -radius ~a ~]~@[ -width ~a ~]~@[ -filled ~a ~]~@[ -fillcolour ~a ~]~@[ -style ~(~a~) ~]~@[ -smooth ~a ~]~@[ -boxwidth ~a ~]~@[ -whiskers ~a ~]~@[ -whiskerswidth ~a ~]~@[ -mediancolour ~(~a~) ~]~@[ -medianwidth ~a~]"
                (name chart) 
                (name chart)
                series
                (or colour color) 
                (and type
                     (when-valid type '("line" "symbol" "both") "data-config - type"))
                (and symbol
                     (when-valid symbol '("plus" "cross" "circle" "up" "down" "dot" "upfilled" "downfilled") "data-config - symbol"))
                radius width
                (and filled
                     (when-valid filled '("no" "up" "down") "data-config - filled"))
                fillcolour
                (and style
                     (when-valid style '("filled" "spike" "symbol" "plateau" "stair") "data-config - style"))
                (and smooth (if smooth "1" "0"))
                boxwidth
                (and whiskers
                     (when-valid style '("extremes" "iqr" "none") "data-config - whiskers"))
                whiskerwidth
                mediancolour
                medianwidth))
  (otherwise
    (error "Unknown chart type ~a passed to data-config" (type-of chart)))))

(defun dot-config (chart series &key colour color scale radius scalebyvalue (outline t) classes effect-3d)
  "* `chart` - to modify
  * `colour` `color` - colour of dot
  * `scale` - scale factor for radius->pixels conversion
  * `radius` - of dot
  * `scalebyvalue` - t/nil to turn scale by value on (default) or off
  * `outline` - t/nil to display outline of dot
  * `classes` - flat pairs of limits and colours (e.g. `'(0 :green 2 :red 3 :blue)`)
  * `effect-3d` - t/nil to turn 3d effect on or off (default)

  Sets the configuration options for drawing dots on plots.

  Error:: if chart type is not one of <<polar-plot-class>> <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"
  (typecase chart
    ((or xy-plot-group polar-plot)
     (format-wish "global ~a; $~a dotconfig ~a~@[ -colour ~(~a~) ~]~@[ -scale ~a ~]~@[ -radius ~a ~]~@[ -scalebyvalue ~a ~]~@[ -outline ~a ~]~@[ -classes {~{~a ~}} ~]~@[ -3deffect ~a ~]"
                  (name chart) (name chart) series
                  (or colour color)
                  scale
                  radius
                  (if scalebyvalue "1" "0")
                  outline
                  classes
                  (and effect-3d (if effect-3d "1" "0"))))
    (otherwise
      (error "Unknown chart type ~a passed to dot-config" (type-of chart)))))

(defgeneric draw-box-and-whiskers (chart series xcrd ycrd)
            (:documentation "* `chart` - to modify
                            * `series` - name of series the data belong to
                            * `xcrd` - x-coordinate of box _or_ list of values
                            * `ycrd` - y-coordinate of box _or_ list of values

                            Draw a box and whiskers in the plot. 
                            One of `xcrd` or `ycrd` should be a list, from which the box and whiskers are computed, the other is the point on its axis at which to draw the box.

                            Error:: 
                            
                            * if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>;
                            * unless one of xcrd/ycrd is a list and the other a coordinate."))
(defmethod draw-box-and-whiskers ((chart xy-plot-group) series xcrd ycrd)
  (cond ((and (listp xcrd) (listp ycrd))
         (error "box-and-whiskers: only one of xcrd/ycrd can be a list"))
        ((listp xcrd)
         (format-wish "global ~a; $~a box-and-whiskers ~a {~{~f ~}} ~f"
                      (name chart) (name chart) series xcrd ycrd))
        ((listp ycrd)
         (format-wish "global ~a; $~a box-and-whiskers ~a ~f {~{~f ~}}"
                      (name chart) (name chart) series xcrd ycrd))
        (t
          (error "box-and-whiskers: one of xcrd/ycrd must be a list"))))

(defgeneric draw-circle (chart xc yc radius &optional colour)
            (:documentation "* `chart` - <<isometric-plot-class>> to modify
                            * `xc` - float, x-coordinate of circle centre
                            * `yc` - float, y-coordinate of circle centre
                            * `radius` - float, radius of circle
                            * `colour` - for outline

                            Draw the outline of specified circle on an isometric-plot."))
(defmethod draw-circle ((chart isometric-plot) xc yc radius &optional (colour :black))
  (format-wish "global ~a; $~a plot circle ~f ~f ~f ~(~a~)" 
               (name chart) (name chart) 
               xc yc radius colour))

(defgeneric draw-contour-fill (chart xcrds ycrds values &optional classes)
            (:documentation "* `chart` - to modify
                            * `xcrds` - list of lists, each an x-coordinate of grid cell
                            * `ycrds` - list of lists, each a y-coordinate of grid cell
                            * `values` - list of lists, value at (x, y) grid cell
                            * `classes` - list of class values

                            Draws filled contours for the given values on the grid.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-contour-fill ((chart xy-plot-group) xcrds ycrds values &optional classes)
  (format-wish 
    "global ~a; $~a contourfill {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}} {~{~a ~}}"
    (name chart) (name chart) 
    xcrds ycrds
    values classes))

(defgeneric draw-contour-lines (chart xcrds ycrds values &optional classes)
            (:documentation "* `chart` - to modify
                            * `xcrds` - list of lists, each an x-coordinate of grid cell
                            * `ycrds` - list of lists, each a y-coordinate of grid cell
                            * `values` - list of lists, value at (x, y) grid cell
                            * `classes` - list of class values

                            Draws contour lines for the given values on the grid.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-contour-lines ((chart xy-plot-group) xcrds ycrds values &optional classes)
  (format-wish 
    "global ~a; $~a contourlines {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}} {~{~a ~}}"
    (name chart) (name chart) 
    xcrds ycrds
    values classes))

(defgeneric draw-contour-lines-function-values (chart xvec yvec valuesmat &optional classes)
            (:documentation "* `chart` - to modify
                            * `xvce` - list of x-coordinates in increasing order 
                            * `yvec` - list of y-coordinates in increasing order 
                            * `valuesmat` - list of lists, value at (x, y) grid cell
                            * `classes` - list of class values

                            Draws contour lines for the given values on the grid.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-contour-lines-function-values ((chart xy-plot-group) xvec yvec valuesmat &optional classes)
  (format-wish "global ~a; $~a contourlinesfunctionvalues {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}} {~{~a ~}}"
               (name chart) (name chart) 
               xvec yvec
               valuesmat classes))

(defgeneric draw-dot (chart series xcrd ycrd value)
            (:documentation "* `chart` - to modify
                            * `series` - which dot belongs to
                            * `xcrd` - float, x-coordinate
                            * `ycrd` - float, y-coordinate
                            * `value` - float, value determining size and colour

                            Draws a dot in the given chart.

                            Error:: if chart type is not one of <<polar-plot-class>> <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-dot ((chart xy-plot-group) series xcrd ycrd value)
  "Draws a dot in the given plot"
  (format-wish "global ~a; $~a dot ~a ~f ~f ~f"
               (name chart) (name chart) series xcrd ycrd value))

(defmethod draw-dot ((chart polar-plot) series xcrd ycrd value)
  "Draws a dot in the given plot"
  (format-wish "global ~a; $~a dot ~a ~f ~f ~f"
               (name chart) (name chart) series xcrd ycrd value))

(defgeneric draw-filled-circle (chart xc yc radius &optional colour)
            (:documentation "* `chart` - <<isometric-plot-class>> to modify
                            * `xc` - float, x-coordinate of circle centre
                            * `yc` - float, y-coordinate of circle centre
                            * `radius` - float, radius of circle
                            * `colour` - for outline

                            Draw and fill specified circle on an isometric-plot."))
(defmethod draw-filled-circle ((chart isometric-plot) xc yc radius &optional (colour :black))
  (format-wish "global ~a; $~a plot filled-circle ~f ~f ~f ~(~a~)" 
               (name chart) (name chart) xc yc radius colour))

(defgeneric draw-filled-polygon (chart series coords)
            (:documentation "* `canvas` - <<ternary-diagram-class>> to modify
                            * `series` - name of data series
                            * `coords` - list of triples of coordinates

                            Draws a filled polygon defined by series of coordinates (triplets)."))
(defmethod draw-filled-polygon ((chart ternary-diagram) series coords)
  (format-wish "global ~a; $~a fill ~a {~{~a ~}}"
               (name chart) 
               (name chart) 
               series 
               (reduce #'append coords)))

(defgeneric draw-filled-rectangle (chart x1 y1 x2 y2 &optional colour)
            (:documentation "* `chart` - <<isometric-plot-class>> to modify
                            * `x1` - float, min x-coordinate of rectangle
                            * `y2` - float, min y-coordinate of rectangle
                            * `x1` - float, max x-coordinate of rectangle
                            * `y2` - float, max y-coordinate of rectangle
                            * `colour` - for outline

                            Draw and fill specified rectangle on an isometric-plot."))
(defmethod draw-filled-rectangle ((chart isometric-plot) x1 y1 x2 y2 &optional (colour :black))
  (format-wish "global ~a; $~a plot filled-rectangle ~f ~f ~f ~f ~(~a~)" 
               (name chart) (name chart) x1 y1 x2 y2 colour))

(defgeneric draw-grid (chart xcrds ycrds)
            (:documentation "* `chart` - to modify
                            * `xcrds` - list of lists, each an x-coordinate of grid cell
                            * `ycrds` - list of lists, each a y-coordinate of grid cell

                            Draws grid cells as lines connecting the given coordinates.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-grid ((chart xy-plot-group) xcrds ycrds)
  (format-wish 
    "global ~a; $~a grid {~{{~{~a ~}} ~}} {~{{~{~a ~}} ~}}" 
    (name chart) (name chart) xcrds ycrds))

(defgeneric draw-interval (chart series x-coord ymin ymax &optional ycenter)
            (:documentation "* `chart` - to modify
                            * `series` - name of data series
                            * `x-coord` - float, x-coordinate - or time-coordinate, for <<tx-plot-class>>
                            * `ymin` - float, minimum y-value
                            * `ymax` - float, maximum y-value
                            * `ycenter` - if given, a symbol is drawn in centre

                            Adds a vertical error interval to chart.

                            Error:: if chart type is not one of <<strip-chart-class>> <<tx-plot-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-interval ((chart xy-plot-group) series x-coord ymin ymax &optional ycenter)
  (format-wish "global ~a; $~a interval ~a ~f ~f ~f ~a"
               (name chart) (name chart) series x-coord ymin ymax
               (if ycenter ycenter "")))

(defmethod draw-interval ((chart tx-plot) series x-coord ymin ymax &optional ycenter)
  (format-wish "global ~a; $~a interval ~a ~f ~f ~f ~a"
               (name chart) (name chart) series x-coord ymin ymax
               (if ycenter ycenter "")))

(defgeneric draw-label-dot (chart x y text &optional orientation)
            (:documentation "* `chart` - to modify
                            * `x` - float, x-coordinate of dot
                            * `y` - float, y-coordinate of dot
                            * `text` - to display as label
                            * `orientation` - position of label relative to dot - one of (:n :s :e :w)

                            Draws a label and dot in the given chart: configure using <<data-config-function>> with 'labeldot' as series name.

                            Error:: if chart type is not one of <<polar-plot-class>> <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-label-dot ((chart xy-plot-group) x y text &optional (orientation "n"))
  (format-wish "global ~a; $~a labeldot ~f ~f {~a} ~a"
               (name chart) (name chart) x y text
               (when-valid orientation '("n" "s" "e" "w") "orientation")))

(defmethod draw-label-dot ((chart polar-plot) x y text &optional (orientation "n"))
  (format-wish "global ~a; $~a labeldot ~f ~f {~a} ~a"
               (name chart) (name chart) x y text
               (when-valid orientation '("n" "s" "e" "w") "orientation")))

(defgeneric draw-line (chart series coords)
            (:documentation "* `canvas` - <<ternary-diagram-class>> to modify
                            * `series` - name of data series
                            * `coords` - list of triples of coordinates

                            Draws a continuous line defined by series of coordinates (triplets)."))
(defmethod draw-line ((chart ternary-diagram) series coords)
  (format-wish "global ~a; $~a line ~a {~{~a ~}}"
               (name chart) 
               (name chart) 
               series 
               (reduce #'append coords)))

(defgeneric draw-minmax (chart series xcoord ymin ymax)
            (:documentation "* `chart` - to modify
                            * `series` - which dot belongs to
                            * `xcoord` - float, x-coordinate
                            * `ymin` - float, minimum y-coordinate
                            * `ymax` - float, maximum y-coordinate

                            Draws a filled strip in the given chart.

                            Error:: if chart type is not one of <<polar-plot-class>> <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-minmax ((chart xy-plot-group) series xcoord ymin ymax)
  "Draws a filled strip representing a minimum and maximum"
  (format-wish "global ~a; $~a minmax ~a ~f ~f ~f"
               (name chart) (name chart) series xcoord ymin ymax))

(defmethod draw-minmax ((chart polar-plot) series xcoord ymin ymax)
  "Draws a filled strip representing a minimum and maximum"
  (format-wish "global ~a; $~a minmax ~a ~f ~f ~f"
               (name chart) (name chart) series xcoord ymin ymax))

(defgeneric draw-rectangle (chart x1 y1 x2 y2 &optional colour)
            (:documentation "* `chart` - <<isometric-plot-class>> to modify
                            * `x1` - float, min x-coordinate of rectangle
                            * `y2` - float, min y-coordinate of rectangle
                            * `x1` - float, max x-coordinate of rectangle
                            * `y2` - float, max y-coordinate of rectangle
                            * `colour` - for outline

                            Draw the outline of specified rectangle on an isometric-plot."))
(defmethod draw-rectangle ((chart isometric-plot) x1 y1 x2 y2 &optional (colour :black))
  (format-wish "global ~a; $~a plot rectangle ~f ~f ~f ~f ~(~a~)" 
               (name chart) (name chart) x1 y1 x2 y2 colour))

(defgeneric draw-region (chart series xlist ylist)
            (:documentation "* `chart` - to modify
                            * `series` - which polygon belongs to
                            * `xlist` - floats, x-coordinates of points
                            * `ylist` - floats, y-coordinates of points

                            Draws a filled polygon on the given chart.

                            Error:: if chart type is not one of <<polar-plot-class>> <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-region ((chart xy-plot-group) series xlist ylist)
  (format-wish "global ~a; $~a region ~a {~{~a ~}} {~{~a ~}}"
               (name chart)
               (name chart)
               series
               xlist
               ylist))

(defmethod draw-region ((chart polar-plot) series xlist ylist)
  (format-wish "global ~a; $~a region ~a {~{~a ~}} {~{~a ~}}"
               (name chart)
               (name chart)
               series
               xlist
               ylist))

(defgeneric draw-ticklines (chart &optional colour)
            (:documentation "* `chart` - <<ternary-diagram-class>> to modify
                            * `colour` - to draw the lines

                            Turns on the ticklines, with optional colour."))
(defmethod draw-ticklines ((chart ternary-diagram) &optional (colour :black))
  (format-wish "global ~a; $~a ticklines ~(~a~)" 
               (name chart) (name chart) colour))

(defgeneric draw-trendline (chart series x-coord y-coord)
            (:documentation "* `chart` - to modify
                            * `series` - which data belong to
                            * `x-coord` - float, x-coordinate
                            * `y-coord` - float, y-coordinate

                            Draws a trend line on xy-style plots.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-trendline ((chart xy-plot-group) series x-coord y-coord)
  "Trend line for xy-style plots"
  (format-wish "global ~a; $~a trend ~a ~f ~f"
               (name chart) (name chart) series x-coord y-coord))

(defgeneric draw-vector (chart series x-coord y-coord ucmp vcmp)
            (:documentation "* `chart` - to modify
                            * `series` - which data belong to
                            * `x-coord` - float, x-coordinate of arrow
                            * `y-coord` - float, y-coordinate of arrow
                            * `ucmp` - x-component / length of vector
                            * `vcmp` - y-component / angle of vector

                            Draws a vector (line with arrow) on xy-style plots.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod draw-vector ((chart xy-plot-group) series xcrd ycrd ucmp vcmp)
  (format-wish "global ~a; $~a vector ~a ~f ~f ~f ~f"
               (name chart) (name chart) series xcrd ycrd ucmp vcmp))

(defun draw-vertical-line (chart text time-point &key color colour dash fill width)
  "* `canvas` - <<gantt-chart-class>> <<time-chart-class>> or <<status-timeline-class>> to modify
  * `text` - to display at top of line
  * `time-point` - at which to display the line
  * `colour` `color` - of line

  Options for <<status-timeline-class>> only:

  * `dash` - dash pattern for line, one of (:lines :dots1 :dots2 :dots3 :dots4 :dots5)
  * `fill` - fill pattern for line
  * `width` - of line

  Adds a vertical line to a chart.
  
  Error:: if chart type not recognised."
  (typecase chart
    ((or gantt-chart time-chart)
     (when (or dash fill width)
       (warn "Chart type ~a does not support 'dash' 'fill' 'width' options"
             (type-of chart)))
     (format-wish "global ~a; $~a vertline {~a} {~a} ~@[~(~a~)~]" 
                  (name chart) (name chart) text time-point (or color colour)))
    (status-timeline
      (format-wish "global ~a; $~a vertline {~a} ~f~@[ -colour ~(~a~) ~]~@[ -dash ~(~a~) ~]~@[ -fill ~(~a~) ~]~@[ -width ~a ~]"
                   (name chart) (name chart) text time-point
                   (or color colour) dash fill width))
    (otherwise
      (error "Unknown chart type ~a passed to vertical-line" (type-of chart)))))

(defgeneric draw-x-band (chart ymin ymax)
            (:documentation "* `chart` - to modify
                            * `ymin` - float, lower bound
                            * `ymax` - float, upper bound

                            Draws a horizontal light-grey band."))
(defmethod draw-x-band ((chart plotchart) ymin ymax)
  (format-wish "global ~a; $~a xband ~f ~f" (name chart) (name chart) ymin ymax))

(defgeneric draw-y-band (chart xmin xmax)
            (:documentation "* `chart` - to modify
                            * `ymin` - float, lower bound
                            * `ymax` - float, upper bound

                            Draws a vertical light-grey band."))
(defmethod draw-y-band ((chart plotchart) xmin xmax)
  (format-wish "global ~a; $~a yband ~f ~f" (name chart) (name chart) xmin xmax))

(defgeneric explode (chart segment)
            (:documentation "* `chart` - <<pie-chart-class>> to modify
                            * `segment` - an integer index for the slice to draw outside the main circle

                            Displays segment number out of the circle."))
(defmethod explode ((chart pie-chart) segment)
  (format-wish "global ~a; $~a explode ~d" (name chart) (name chart) segment))

(defgeneric font (chart keyword newfont)
            (:documentation "* `chart` - <<gantt-chart-class>> to modify
                            * `keyword` - part of chart to change - one of (:description :summary :scale)
                            * `newfont` - name of font 

                            Changes font of part of gantt chart."))
(defmethod font ((chart gantt-chart) keyword newfont)
  (format-wish "global ~a; $~a font ~a {~a}" 
               (name chart) 
               (name chart) 
               (when-valid keyword '("description" "summary" "scale") "font - keyword")
               newfont))

(defgeneric gridsize (chart nxcells nycells)
            (:documentation "* `chart` - <<threed-plot-class>> to modify
                            * `nxcells` - integer > 0 of number of grid cells in x-direction
                            * `nycells` - integer > 0 of number of grid cells in y-direction

                            Sets grid size in two dimensions."))
(defmethod gridsize ((chart threed-plot) nxcells nycells)
  (format-wish "global ~a; $~a gridsize ~d ~d" (name chart) (name chart) nxcells nycells))

(defun horizontal-scrollbar (chart hscroll)
  "* `chart` - <<gantt-chart-class>> or <<time-chart-class>> to modify
  * `hscroll` - scroll bar to attach to chart
  
  Connect a horizontal scrollbar to chart."
  (typecase chart
    ((or gantt-chart time-chart)
     (format-wish "global ~a; $~a hscroll ~a" (name chart) (name chart) (name hscroll)))
    (otherwise
      (error "Unknown chart type ~a passed to horizontal-scrollbar" (type-of chart)))))

(defgeneric interpolate-data (chart data contours)
            (:documentation "* `chart` - <<threed-plot-class>> to modify
                            * `data` - list of lists: list of rows of x-values 
                            * `contours` - list of values in ascending order defining countour boundaries

                            Plots given list-of-lists data with interpolated contours."))
(defmethod interpolate-data ((chart threed-plot) data contours)
  (format-wish 
    "global ~a; $~a interpolatedata {~{{~{~a ~}} ~}} {~{~a ~}}"
    (name chart)
    (name chart)
    data
    contours))

(defgeneric legend (chart series text &optional spacing)
            (:documentation "* `chart` - to modify
                            * `series` - name of data series
                            * `text` - string to display
                            * `spacing` - integer, vertical spacing

                            Adds an entry to the chart legend"))
(defmethod legend ((chart plotchart) series text &optional spacing)
  (format-wish "global ~a; $~a legend ~a {~a} ~@[~d ~]" 
               (name chart) (name chart) series text spacing))

(defgeneric legend-config (chart &key background border canvas font legend-type position spacing)
            (:documentation "* `chart` - to modify
                            * `background` - name of background colour
                            * `border` - name of border colour
                            * `canvas` - reference to canvas
                            * `font` - name of font
                            * `legend-type` - one of (:rectangle :line)
                            * `position` - one of (:top-left :top-right :bottom-left :bottom-right)
                            * `spacing` - integer, vertical spacing

                            Configures the chart legend"))
(defmethod legend-config ((chart plotchart) &key background border canvas font legend-type position spacing)
  (format-wish "global ~a; $~a legendconfig~@[ -background ~(~a~) ~]~@[ -border ~(~a~) ~]~@[ -canvas ~a ~]~@[ -font {~a} ~]~@[ -legendtype ~(~a~) ~]~@[ -position ~(~a~) ~]~@[ -spacing ~d ~]" 
               (name chart) 
               (name chart) 
               background border (and canvas (widget-path canvas)) font 
               (and legend-type 
                    (when-valid legend-type '("rectangle" "line") "legend-config - legend-type"))
               (and position 
                    (when-valid position '("top-left" "top-right" "bottom-left" "bottom-right") "legend-config - position"))
               spacing))

(defgeneric legend-isometric-lines (chart values classes)
            (:documentation "* `chart` - to modify
                            * `values` - list of lists, value at (x, y) grid cell as used in drawing
                            * `classes` - list of class values as used in drawing

                            Adds contour classes to the legend as coloured lines.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod legend-isometric-lines ((chart xy-plot-group) values classes)
  (format-wish "global ~a; $~a legendisolines {~{~a ~}} {~{~a ~}}" 
               (name chart) (name chart) values classes))

(defgeneric legend-remove (chart series)
            (:documentation "* `chart` - to modify
                            * `series` - name of data series

                            Removes series from legend and redraws it"))
(defmethod legend-remove ((chart plotchart) series)
  (format-wish "global ~a; $~a removefromlegend {~a}" (name chart) (name chart) series))

(defgeneric legend-shades (chart values classes)
            (:documentation "* `chart` - to modify
                            * `values` - list of lists, value at (x, y) grid cell as used in drawing
                            * `classes` - list of class values as used in drawing

                            Adds contour classes to the legend as coloured rectangles.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod legend-shades ((chart xy-plot-group) values classes)
  (format-wish "global ~a; $~a legendshades {~{~a ~}} {~{~a ~}}" 
               (name chart) (name chart) values classes))

(defun milestone (chart text time &optional (colour :black))
  "* `chart` - <<gantt-chart-class>> or <<time-chart-class>> to add milestone to
  * `time-point` - at which the milestone must be positioned
  * `colour` - of the triangle (defaults to black)

  Adds a milestone to the current row of a <<gantt-chart-class>> or <<time-chart-class>>."
  (typecase chart
    ((or gantt-chart time-chart)
     (format-wish "global ~a; $~a milestone {~a} {~a} ~(~a~)" 
                  (name chart) (name chart) text time colour))
    (otherwise
      (error "Unknown chart type ~a passed to milestone" (type-of chart)))))

(defgeneric period (chart text time-begin time-end &optional colour)
            (:documentation
              "* `chart` - <<gantt-chart-class>> or <<time-chart-class>> to add period to
              * `time-begin` - when period starts
              * `time-end` - when period ends
              * `colour` - of the triangle (defaults to black)

              Adds a new time period to the current row of a <<gantt-chart-class>> or <<time-chart-class>>."))
(defmethod period ((chart time-chart) text time-begin time-end &optional (colour :black))
  (format-wish "global ~a; $~a period {~a} {~a} {~a} ~(~a~)" 
               (name chart) (name chart) text time-begin time-end
               colour))

(defmethod period ((chart gantt-chart) text time-begin time-end &optional (colour :black))
  "Adds a time period to a chart"
  (format-wish "global ~a; $~a period {~a} {~a} {~a} ~(~a~)" 
               (name chart) (name chart) text time-begin time-end
               colour))

(defgeneric plaintext (chart x y text &optional direction)
            (:documentation "* `chart` - to modify
                            * `x` - x-coordinate
                            * `y` - y-coordinate
                            * `text` - string to display
                            * `direction` - one of (:north :north-east :east :south-east :south
                                                            :south-west :west :north-west)

                            Adds plaintext to the plot, at given coordinates.

                            NOTE: not supported for 3D charts."))
(defmethod plaintext ((chart plotchart) x y text &optional (direction "north"))
  (format-wish "global ~a; $~a plaintext ~f ~f {~a} ~a" 
               (name chart) (name chart) x y text 
               (when-valid direction  
                           '("north" "north-east" "east" "south-east" "south"
                             "south-west" "west" "north-west")
                           "direction")))

(defgeneric plaintext-config (chart &key font justify text-colour text-color)
            (:documentation "* `chart` - to modify
                            * `font` - name of font
                            * `justify` - one of (:left :center :right)
                            * `textcolour` `textcolor` - name of text colour

                            Configures plaintext for given plot - settings apply to next call to chart-balloon."))
(defmethod plaintext-config ((chart plotchart) &key font justify text-colour text-color)
  (format-wish "global ~a; $~a plaintextconfig~@[ -font {~a} ~]~@[ -justify ~(~a~) ~]~@[ -textcolour ~(~a~) ~]" 
               (name chart) 
               (name chart) 
               font
               (and justify
                    (when-valid justify '("left" "center" "right") "plaintext-config - justify"))
               (or text-colour text-color)))

(defun plot (chart &rest args)
  "Generic call to plot function, with arguments varying based on chart type:

  <<bar-chart-class>> or <<horizontal-bar-chart-class>>:: plot (*chart* *series* *data* *colour* &optional *direction* *brightness*)
  * `chart` - to modify
  * `series` - name of series the data belong to
  * `data` - values of series, one per label
  * `colour` - colour of the bars
  * `direction` - one of (:uniform :left-right :right-left) for colour change
  * `brightness` - one of (:bright :dark)

  <<box-plot-class>>:: plot (*chart* *series* *label* *values*)
  * `chart` - to modify
  * `series` - name of data series
  * `label` - label along x or y axis that data belong to
  * `values` - list of values from which box-and-whiskers data are extracted

  <<histogram-class>>:: plot (*chart* *series* *x-coord* *y-coord*)
  * `chart` - to modify
  * `series` - name of series the data belong to
  * `x-coord` - x coordinate of bar - defines the _right_ edge
  * `y-coord` - y coordinate, or height, of bar

  <<pie-chart-class>> or <<spiral-pie-class>>:: plot (*chart* *data*)
  * `chart` - to modify
  * `data` - list of `(label value)` pairs, defining each slice in the pie chart.
  The value is used to control the _angle_ of the pie slice in a <<pie-chart-class>> 
  or _radius_ of the slice in a <<spiral-pie-class>>.

  <<polar-plot-class>> <<right-axis-class>> <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>:: plot (*chart* *series* *x-coord* *y-coord*)
  * `chart` - to modify
  * `series` - name of series the data belong to
  * `x-coord` - float, x-coordinate of point to plot
  * `y-coord` - float, y-coordinate of point to plot

  <<radial-chart-class>>:: plot (*chart* *data* *colour* &optional *thickness*)
  * `chart` - to modify
  * `data` - list of values, one per spoke
  * `colour` - for this set of data
  * `thickness` - for width of line

  <<status-timeline-class>>:: plot (*chart* *series* *start* *stop* &optional *colour*)
  * `chart` - to modify
  * `series` - name of series the data belong to
  * `start` - float, start time (x-coordinate)
  * `stop` - float, end time (x-coordinate)
  * `colour` - colour of bar

  Draws a bar from start to stop points.

  <<ternary-diagram-class>>:: plot (*chart* *series* *xcrd* *ycrd* *zcrd* *text* &optional *direction*)
  * `chart` - to modify
  * `series` - name of data series
  * `xcrd` - x coordinate
  * `ycrd` - y coordinate
  * `zcrd` - z coordinate
  * `text` - text to display
  * `direction` - one of (:n :s :e :w :ne :nw :se :sw) - placement of text relative to point

  <<threed-bar-chart-class>>:: plot (*chart* *label* *yvalue* *colour*)
  * `chart` - to modify
  * `label` - shown below bar
  * `yvalue` - height of bar 
  * `colour` - colour of the bar

  <<threed-ribbon-plot-class>>:: plot (*chart* *yzpairs*)
  * `chart` - to modify
  * `yzpairs` - list of pairs of (y, z) coordinates

  <<tx-plot-class>>:: plot (*chart* *series* *time-coord* *x-coord*)
  * `chart` - to modify
  * `series` - name of data series
  * `time-coord` - time coordinate for value
  * `x-coord` - x-coord for value

  Plots a point at (time-coord, x-coord) on chart.

  <<windrose-class>>:: plot (*chart* *data* *colour*)
  * `chart` - to modify
  * `data` - list of data values, defining distance from centre
  * `colour` - name of the bar colour

  Error:: if `chart` type not recognised."
  (typecase chart
    ((or bar-chart horizontal-bar-chart)
     (if (member (length args) '(3 4 5))
       (format-wish "global ~a; $~a plot ~a {~{~a ~}} ~(~a~) ~(~a~) ~(~a~)"
                    (name chart)
                    (name chart)
                    (first args)
                    (second args)
                    (third args)
                    (if (>= (length args) 4) (fourth args) "")
                    (if (= (length args) 5) (fifth args) ""))
       (error "bar-chart:plot series xdata colour &optional direction brightness")))

    (box-plot
      (if (and (= 3 (length args))
               (listp (third args)))
        (format-wish "global ~a; $~a plot ~a {~a} {~{~a ~}}" 
                     (name chart) (name chart)
                     (first args) (second args) (third args))
        (error "box-plot:plot series label values")))

    (histogram
      (if (= 3 (length args))
        (format-wish "global ~a; $~a plot ~a ~f ~f" 
                     (name chart) (name chart) (first args) (second args) (third args))
        (error "histogram:plot series x-coord y-coord")))

    ((or pie-chart spiral-pie)
     (if (and (= 1 (length args))
              (listp (first args)))
       (format-wish "global ~a; $~a plot {~{{~a} ~}}" 
                    (name chart) (name chart) (reduce #'append (first args)))
       (error "plot requires 1 list argument for pie-chart")))

    ((or polar-plot right-axis xy-plot-group)
     (if (= 3 (length args))
       (format-wish "global ~a; $~a plot ~a ~f ~f" 
                    (name chart) (name chart) (first args) (second args) (third args))
       (error "plot requires 3 arguments for ~a" (type-of chart))))

    (radial-chart
      (if (member (length args) '(2 3))
        (format-wish "global ~a; $~a plot {~{~a ~}} ~(~a~) ~a" 
                     (name chart) (name chart)
                     (first args)
                     (second args)
                     (if (= 3 (length args)) (third args) ""))
        (error "radial-chart:plot data colour &optional thickness")))

    (status-timeline
      (if (member (length args) '(3 4))
        (format-wish "global ~a; $~a plot ~a ~f ~f ~(~a~)"
                     (name chart) (name chart)
                     (first args) (second args) (third args) 
                     (if (= 4 (length args)) (fourth args) ""))
        (error "status-timeline:plot series start stop &optional colour")))

    (ternary-diagram
      (if (member (length args) '(5 6))
        (format-wish "global ~a; $~a plot ~a ~f ~f ~f {~a}~@[ ~a~]"
                     (name chart) (name chart)
                     (first args) (second args) (third args) (fourth args) (fifth args)
                     (and (= 6 (length args)) 
                          (when-valid (sixth args) 
                                      '("n" "s" "e" "w" "nw" "ne" "sw" "se")
                                      "plot -ternary-diagram position")))
        (error "ternary-diagram:plot series xcrd ycrd zcrd text &optional direction")))

    (threed-bar-chart
      (if (= 3 (length args))
        (format-wish "global ~a; $~a plot ~a ~a ~(~a~)" 
                     (name chart) 
                     (name chart) 
                     (first args)
                     (second args)
                     (third args))
        (error "threed-bar-chart:plot label yvalue colour")))

    (threed-ribbon-plot
      (if (= 1 (length args))
        (format-wish 
          "global ~a; $~a plot {~{{~{~a ~}} ~}}"
          (name chart)
          (name chart)
          (first args))
        (error "threed-ribbon-plot:plot yzpairs")))

    (tx-plot
      (if (and (= 3 (length args)) (stringp (second args)) (numberp (third args)))
        (format-wish "global ~a; $~a plot ~a ~a ~f" 
                     (name chart) (name chart) (first args) (second args) (third args))
        (error "plot requires 3 arguments (series time-coord x-coord) for tx-plot, not ~a" args)))

    (windrose
      (if (= 2 (length args))
        (format-wish "global ~a; $~a plot {~{~a ~}} ~(~a~)"
                     (name chart) (name chart) (first args) (second args))
        (error "windrose:plot data colour")))

    (otherwise
      (error "Unknown chart type passed to plot"))))

(defun plot-config (charttype component property value)
  "* `charttype` - identifier for type of chart
  * `component` - component of chart to configure - one of (:leftaxis :rightaxis :background :margin) etc.
  * `property` - of component
  * `value` - to set for property

  Sets a new value for given charttype-component-property.

  Details on the components and properties can be found in the tk documentation
  for
  https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#145[plotconfig]
  and
  https://core.tcl-lang.org/tklib/doc/trunk/embedded/www/tklib/files/modules/plotchart/plotchart.html#146[plotstyle]."
  (format-wish "::Plotchart::plotconfig ~(~a~) ~(~a~) ~(~a~) ~(~a~)"
               charttype component property value))

(defgeneric plot-cumulative (chart series x-coord y-coord)
            (:documentation "* `chart` - <<histogram-class>> to modify
            * `series` - name of series the data belong to
            * `x-coord` - x coordinate of bar - defines the _right_ edge
            * `y-coord` - y coordinate, or height, of bar

            Adds a data point to the <<histogram-class>>, accumulating previous points."))
(defmethod plot-cumulative ((chart histogram) series x-coord y-coord)
  (format-wish "global ~a; $~a plotcumulative ~a ~f ~f" 
               (name chart) (name chart) series x-coord y-coord))

(defgeneric plot-data (chart data)
            (:documentation "* `chart` - <<threed-plot-class>> to modify
                            * `data` - list of lists: list of rows of x-values 

                            Plots given data."))
(defmethod plot-data ((chart threed-plot) data)
  (format-wish "global ~a; $~a plotdata {~{{~{~a ~}} ~}}"
               (name chart) (name chart) data))

(defgeneric plot-erase (chart) 
            (:documentation "* `chart` - to erase

                            Erases given chart and all associated resources."))
(defmethod plot-erase ((chart plotchart))
  (format-wish "::Plotchart::eraseplot $~a" (name chart)))

(defgeneric plot-list (chart series xlist ylist &optional every)
            (:documentation "* `chart` - to modify
                            * `series` - name of data series
                            * `xlist` - list of x-coordinates 
                            * `ylist` - list of y-coordinates 
                            * `every` - t/nil to indicate how often symbol should be drawn

                            Draws a series of data as a whole.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod plot-list ((chart xy-plot-group) series xlist ylist &optional every)
  (format-wish "global ~a; $~a plotlist {~a} {~{~a ~}} {~{~a ~}}~@[ ~a~]"
               (name chart)
               (name chart)
               series
               xlist
               ylist
               every))

(defgeneric plot-pack (canvas direction &rest charts)
            (:documentation "* `canvas` - to place the charts onto
                            * `direction` - one of (:top :left :bottom :right)
                            * `charts` - one or more charts to place

                            Copies contents of the given charts onto one canvas."))
(defmethod plot-pack ((canvas canvas) direction &rest charts)
  (format-wish "::Plotchart::plotpack ~a ~a ~{ $~a~}"
               (widget-path canvas)
               (when-valid direction '("top" "left" "bottom" "right") "direction")
               (mapcar #'name charts)))

(defgeneric rchart (chart series xcoord ycoord)
            (:documentation "* `canvas` - to modify
                            * `series` - name of data series
                            * `xcoord` - float, x-coordinate of point to plot
                            * `ycoord` - float, y-coordinate of point to plot

                            Using `rchart` instead of `plot` shows +/- 1 s.d. line around data.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod rchart ((chart xy-plot-group) series xcoord ycoord)
  (format-wish "global ~a; $~a rchart ~a ~f ~f"
               (name chart) (name chart) series xcoord ycoord))

(defgeneric ribbon (chart yzpairs)
            (:documentation "* `chart` - <<threed-plot-class>> to modify
                            * `yzpairs` - list of pairs of (y, z) coordinates

                            Plots a ribbon based on given yz-pairs."))
(defmethod ribbon ((chart threed-plot) yzpairs)
  (format-wish "global ~a; $~a ribbon {~{{~{~a ~}} ~}}"
               (name chart) (name chart) yzpairs))

(defgeneric save-plot (chart filename &key plotregion)
            (:documentation "* `chart` - to use
                            * `filename` - name of file to save to (postscript only supported)
                            * `plotregion` - one of (:bbox :window)

                            Saves the chart to a postscript file."))
(defmethod save-plot ((chart plotchart) filename &key (plotregion :window))
  (format-wish "global ~a; $~a saveplot {~a} -plotregion ~(~a~)" 
               (name chart) 
               (name chart) 
               filename 
               (when-valid plotregion '("bbox" "window") "save-plot - plotregion")))

(defgeneric separator (chart)
            (:documentation "* `chart` - <<table-chart-class>> to modify

                            Adds a separating line to table chart."))
(defmethod separator ((chart table-chart))
  (format-wish "global ~a; $~a separator" (name chart) (name chart)))

(defgeneric subtitle (chart title)
            (:documentation "* `chart` - to modify
                            * `title` - string to display

                            Sets the chart subtitle."))
(defmethod subtitle ((chart plotchart) title)
  (format-wish "global ~a; $~a subtitle {~a}" (name chart) (name chart) title))

(defgeneric summary (chart text &rest args)
            (:documentation "* `chart` - <<gantt-chart-class>> to modify
                            * `text` - text summarising the tasks
                            * `args` - one or more tasks, as returned from <<task-function>>

                            Adds summary text covering given tasks."))
(defmethod summary ((chart gantt-chart) text &rest args)
  (format-wish "global ~a; $~a summary {~a} ~{ $~a~}" 
               (name chart) (name chart) text args))

(defgeneric task (chart text time-begin time-end completed)
            (:documentation "* `chart` - <<gantt-chart-class>> to modify
                            * `text` - summary to display on left-side of chart
                            * `time-begin` - the start time
                            * `time-end` - the end time 
                            * `completed` - percentage of task completed, integer from 0 to 100

                            Adds a task with its time period and level of completion to the chart.
                            Returns a task-name (Tcl variable name) which can be passed to <<connect-function>> or <<summary-function."))
(defmethod task ((chart gantt-chart) text time-begin time-end completed)
  (let ((task-name (new-name)))
    (format-wish "global ~a; set ~a [$~a task {~a} {~a} {~a} ~f]" 
                 (name chart) task-name
                 (name chart) text time-begin time-end completed)
    task-name))

(defgeneric title-text (chart title &optional placement)
            (:documentation
              "* `chart` - to modify
              * `title` - text of title
              * `placement` - position of title - one of (:center :left :right)

              Sets title of the whole chart."))
(defmethod title-text ((chart plotchart) title &optional (placement "center"))
  (format-wish "global ~a; $~a title {~a} ~(~a~)" 
               (name chart) (name chart) title 
               (when-valid placement '("center" "left" "right") "title-text: placement")))

(defgeneric v-subtext (chart text)
            (:documentation "* `chart` - to modify
                            * `text` - string to display

                            Sets the subtext of the (vertical) y-axis, and displays vertically along axis."))
(defmethod v-subtext ((chart plotchart) text)
  (format-wish "global ~a; $~a vsubtext {~a}" (name chart) (name chart) text))

(defgeneric v-text (chart text)
            (:documentation
              "* `chart` - to modify
              * `text` - text to draw on axis

              Sets title of the (vertical) y-axis, and draws it vertically."))
(defmethod v-text ((chart plotchart) text)
  "Sets the title of the (vertical) y-axis, and displays vertically along axis"
  (format-wish "global ~a; $~a vtext {~a}" (name chart) (name chart) text))

(defgeneric vector-config (chart series &key colour color scale centred type)
            (:documentation "* `chart` - to modify
                            * `series` - name of data series
                            * `colour` `color` - colour of arrow
                            * `scale` - scale factor to convert arrow size to pixels
                            * `centred` - t/nil to indicate if coordinates refer to arrow start or centre
                            * `type` - interpretation of coordinates - one of (:cartesian :polar :nautical)

                            Configuration options for drawing vectors on xy-plots.

                            Error:: if chart type is not one of <<strip-chart-class>> <<xy-plot-class>> <<x-logy-plot-class>> <<logx-y-plot-class>> <<logx-logy-plot-class>>"))
(defmethod vector-config ((chart xy-plot-group) series &key colour color scale centred centered type)
  (format-wish "global ~a; $~a vectorconfig ~a~@[ -colour ~a ~]~@[ -scale ~a ~]~@[ -centred ~a ~]~@[ -type ~a ~]"
               (name chart) (name chart) series
               (or colour color) scale (or centred centered) 
               (and type
                    (when-valid type 
                                '("cartesian" "polar" "nautical")
                                "vector-config type"))))

(defun vertical-scrollbar (chart vscroll)
  "* `chart` - <<gantt-chart-class>> or <<time-chart-class>> to modify
  * `vscroll` - scroll bar to attach to chart
  
  Connect a vertical scrollbar to chart."
  (typecase chart
    ((or gantt-chart time-chart)
     (format-wish "global ~a; $~a vscroll ~a" (name chart) (name chart) (name vscroll)))
    (otherwise
      (error "Unknown chart type ~a passed to vertical-scrollbar" (type-of chart)))))

(defgeneric x-config (chart &key format tick-length tick-lines minor-ticks label-offset scale)
            (:documentation "* `chart` - to modify
                            * `format` - tcl format string for numbers, see https://www.tcl.tk/man/tcl8.5/TclCmd/format.htm[manual]
                            * `tick-length` - in pixels, length of tick marks
                            * `tick-lines` - t/nil, whether to draw tick lines
                            * `minor-ticks` - number of minor tickmarks
                            * `label-offset` - in pixels, space between label and tickmark
                            * `scale` - (min max step) axis-definition

                            Configure options for x-axis."))
(defmethod x-config ((chart plotchart) &key format tick-length tick-lines minor-ticks label-offset scale)
  (when (or (null scale) (valid-axis-p scale "x-config - scale"))
    (format-wish "global ~a; $~a xconfig~@[ -format {~a} ~]~@[ -ticklength ~d ~]~@[ -ticklines ~a ~]~@[ -minorticks ~a ~]~@[ -labeloffset ~a ~]~@[ -scale {~{~a ~}}~]"
                 (name chart) (name chart)
                 format tick-length (and tick-lines (if tick-lines "1" "0"))
                 minor-ticks label-offset scale)))

(defgeneric x-subtext (chart text)
            (:documentation "* `chart` - to modify
                            * `text` - string to display

                            Sets the subtext of the (horizontal) x-axis."))
(defmethod x-subtext ((chart plotchart) text)
  (format-wish "global ~a; $~a xsubtext {~a}" (name chart)(name chart) text))

(defgeneric x-text (chart text)
            (:documentation
              "* `chart` - to modify
              * `text` - text to draw on axis

              Sets title of the (horizontal) x-axis"))
(defmethod x-text ((chart plotchart) text)
  (format-wish "global ~a; $~a xtext {~a}" (name chart) (name chart) text))

(defgeneric x-ticklines (chart &optional colour dash)
            (:documentation
              "* `chart` - to modify
              * `colour` - colour of the lines (defaults to black)
              * `dash` - dash pattern for lines, one of (:lines :dots1 :dots2 :dots3
                                                                 :dots4 :dots5)

              Draw vertical ticklines at each tick location"))
(defmethod x-ticklines ((chart plotchart) &optional (colour :black) (dash :lines))
  (format-wish "global ~a; $~a xticklines ~(~a~) ~(~a~)"
               (name chart)
               (name chart)
               colour
               (when-valid dash '("lines" "dots1" "dots2" "dots3" "dots4" "dots5") "x-ticklines - dash")))

(defgeneric y-config (chart &key format tick-length tick-lines minor-ticks label-offset scale)
            (:documentation "* `chart` - to modify
                            * `format` - tcl format string for numbers, see https://www.tcl.tk/man/tcl8.5/TclCmd/format.htm[manual]
                            * `tick-length` - in pixels, length of tick marks
                            * `tick-lines` - t/nil, whether to draw tick lines
                            * `minor-ticks` - number of minor tickmarks
                            * `label-offset` - in pixels, space between label and tickmark
                            * `scale` - (min max step) axis-definition

                            Configure options for y-axis."))
(defmethod y-config ((chart plotchart) &key format tick-length tick-lines minor-ticks label-offset scale)
  (when (or (null scale) (valid-axis-p scale "y-config - scale"))
    (format-wish "global ~a; $~a yconfig~@[ -format {~a} ~]~@[ -ticklength ~d ~]~@[ -ticklines ~a ~]~@[ -minorticks ~a ~]~@[ -labeloffset ~a ~]~@[ -scale {~{~a ~}}~]"
                 (name chart)
                 (name chart)
                 format tick-length (and tick-lines (if tick-lines "1" "0"))
                 minor-ticks label-offset scale)))

(defgeneric y-subtext (chart text)
            (:documentation "* `chart` - to modify
                            * `text` - string to display

                            Sets the subtext of the (vertical) y-axis."))
(defmethod y-subtext ((chart plotchart) text)
  (format-wish "global ~a; $~a ysubtext {~a}" (name chart) (name chart) text))

(defgeneric y-text (chart text)
            (:documentation
              "* `chart` - to modify
              * `text` - text to draw on axis

              Sets title of the (vertical) y-axis"))
(defmethod y-text ((chart plotchart) text)
  (format-wish "global ~a; $~a ytext {~a}" (name chart) (name chart) text))

(defgeneric y-ticklines (chart &optional colour dash)
            (:documentation
              "* `chart` - to modify
              * `colour` - colour of the lines (defaults to black)
              * `dash` - dash pattern for lines, one of (:lines :dots1 :dots2 :dots3
                                                                 :dots4 :dots5)

              Draw horizontal ticklines at each tick location"))
(defmethod y-ticklines ((chart plotchart) &optional (colour :black) (dash :lines))
  (format-wish "global ~a; $~a yticklines ~(~a~) ~(~a~)"
               (name chart)
               (name chart)
               colour
               (when-valid dash '("lines" "dots1" "dots2" "dots3" "dots4" "dots5") "y-ticklines - dash")))

;; ---------------------------------------------------------------------------
;; Utility functions - internal use only

(defun when-valid (value valid-values name)
  "Converts value and then checks it is a valid value before returning it - errors if not"
  (let ((result (string-downcase ; this is only used for tcl options, so use lowercase
                  (string value))))
    (if (member result valid-values :test #'string=)
      result
      (error "Value ~a is not a valid ~a" result name))))

(defun valid-axis-p (axis name)
  "Checks the given axis is a valid len-item list of numbers - throws error if invalid"
  (or (and (listp axis)
           (= 3 (length axis))
           (numberp (first axis))
           (numberp (second axis))
           (or (numberp (third axis))
               (string= "" (third axis))))
      (error "Given axis ~a is not a valid axis for ~a" axis name)))

(defun valid-number-pair-p (data name)
  "Checks data is a valid 2-item list of numbers - throws error if invalid"
  (or (and (listp data)
           (= 2 (length data))
           (numberp (first data))
           (numberp (second data)))
      (error "Given data ~a is not valid for ~a" data name)))

(defun valid-time-axis-p (axis name)
  "Checks the given axis is a valid 3-item list of two strings and a number - throws error if invalid"
  (or (and (listp axis)
           (= 3 (length axis))
           (stringp (first axis))
           (stringp (second axis))
           (numberp (third axis)))
      (error "Given axis ~a is not a valid time axis for ~a" axis name)))


